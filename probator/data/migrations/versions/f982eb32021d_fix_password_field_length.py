"""fix password field length

Revision ID: f982eb32021d
Revises: 345a41c69ba1
Create Date: 2019-01-24 09:51:12.117228

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql


# revision identifiers, used by Alembic.
revision = 'f982eb32021d'
down_revision = '345a41c69ba1'


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column(
        'users',
        'password',
        existing_type=mysql.VARCHAR(length=73),
        type_=sa.String(length=100),
        existing_nullable=True
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column(
        'users',
        'password',
        existing_type=sa.String(length=100),
        type_=mysql.VARCHAR(length=73),
        existing_nullable=True
    )
    # ### end Alembic commands ###
